package com.privin.moengagetask.data.remote

import android.os.AsyncTask
import android.os.Handler
import android.os.HandlerThread
import org.json.JSONObject
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection


class NetworkManager(private val delegate: NetworkDelegate): AsyncTask<String, Unit, JSONObject>() {

    companion object{
        const val BASE_URL = "https://candidate-test-data-moengage.s3.amazonaws.com"
        const val FETCH_NEWS = "/Android/news-api-feed/staticResponse.json"
    }


    interface NetworkDelegate{
        fun onSuccess(jsonObject: JSONObject)
        fun onError(msg: String)
    }

    lateinit var urlConnection : HttpURLConnection

    override fun onPreExecute() {
        super.onPreExecute()
    }
    override fun doInBackground(vararg p0: String?): JSONObject? {
        println("NetworkManager doInBackground")
        try {
            // get url from string
            val urlString = BASE_URL+ FETCH_NEWS
            println("NetworkManager host string: $urlString")
            val url = URL(urlString)
            println("NetworkManager host: ${url.host}")
            // create connection
            urlConnection = url.openConnection() as HttpURLConnection
            urlConnection.setRequestProperty("Content-Type", "application/json")
            urlConnection.requestMethod = "GET"
            when (urlConnection.responseCode) {
                200 -> {
                    println("NetworkManager success")
                    return convertToJson(urlConnection.inputStream)
                }
                else -> println("NetworkManager failed "+ urlConnection.responseCode +", msg: "+ urlConnection.responseMessage)
            }
        }catch (exception : Exception){
                println("NetworkManager exception : ${exception.localizedMessage}")
        }
        return null
    }

    override fun onPostExecute(result: JSONObject?) {
        super.onPostExecute(result)
        println("NetworkManager onPostExecute $result")
        result?.let {
            delegate.onSuccess(it)
        }

    }

    private fun convertInputStreamToString(inputStream: InputStream): String {
        val bufferedReader = BufferedReader(InputStreamReader(inputStream))
        val sb = StringBuilder()
        var line: String?
        try {
            while (bufferedReader.readLine().also { line = it } != null) {
                sb.append(line)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return sb.toString()
    }

    private fun convertToJson(inputStream: InputStream) : JSONObject{
        val jsonObj = JSONObject(convertInputStreamToString(inputStream))
        println("NetworkManager json")
        println(jsonObj)
        return jsonObj
    }

}