package com.privin.moengagetask.data

import com.privin.moengagetask.data.model.Article
import com.privin.moengagetask.data.model.Source
import com.privin.moengagetask.data.remote.NetworkManager
import org.json.JSONArray
import org.json.JSONObject

class ArticleRepository(val repoDelegate: RepoDelegate): NetworkManager.NetworkDelegate {

    interface RepoDelegate{
        fun onNewsLoaded(list: List<Article>)
        fun onFailure(msg: String)
    }
    private val networkManager: NetworkManager = NetworkManager(this)

    fun getAllNews(){
        networkManager.execute(NetworkManager.FETCH_NEWS)
    }

    override fun onSuccess(jsonObject: JSONObject) {
        val articles = jsonObject.getJSONArray("articles")
        val articleList = ArrayList<Article>()
        for (i in 0 until articles.length()){
            val articleObject = articles.get(i) as JSONObject
            val sourceObject = articleObject.getJSONObject("source")
            val source = Source(sourceObject.getString("id"),sourceObject.getString("name"))
            val article = Article(source = source,
                author = articleObject.getString("author"),
                title = articleObject.getString("title"),
                description = articleObject.getString("description"),
                url = articleObject.getString("url"),
                urlToImage = articleObject.getString("urlToImage"),
                publishedAt = articleObject.getString("publishedAt"),
                content = articleObject.getString("content"))
            articleList.add(article)
            repoDelegate.onNewsLoaded(articleList)
        }

    }

    override fun onError(msg: String) {
        repoDelegate.onFailure(msg)
    }


}
