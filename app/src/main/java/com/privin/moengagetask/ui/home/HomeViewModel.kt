package com.privin.moengagetask.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.privin.moengagetask.data.ArticleRepository
import com.privin.moengagetask.data.model.Article

class HomeViewModel : ViewModel(), ArticleRepository.RepoDelegate {
    private var articleDataList  : MutableLiveData<List<Article>> = MutableLiveData()
    private var repo : ArticleRepository = ArticleRepository(this)

    fun getArticles() : LiveData<List<Article>>{
        repo.getAllNews()
        return articleDataList
    }

    override fun onCleared() {
        super.onCleared()
    }

    override fun onNewsLoaded(list: List<Article>) {
        articleDataList.value = list
    }

    override fun onFailure(msg: String) {

    }
}
