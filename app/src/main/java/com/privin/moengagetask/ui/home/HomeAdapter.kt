package com.privin.moengagetask.ui.home

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.AsyncTask
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.privin.moengagetask.R
import com.privin.moengagetask.data.model.Article
import kotlinx.android.synthetic.main.adapter_article_item.view.*
import java.lang.Exception
import java.net.URL

class HomeAdapter(val delegate: HomeAdapterDelegate) : RecyclerView.Adapter<HomeAdapter.ArticleItemHolder>() {

    interface HomeAdapterDelegate{
        fun onItemClicked(position: Int)
    }

    private var dataList : List<Article> = ArrayList()

    fun setData(list : List<Article>){
        dataList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleItemHolder {
        val inflator = LayoutInflater.from(parent.context)
        val view = inflator.inflate(R.layout.adapter_article_item,parent,false)
        return ArticleItemHolder(view)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    override fun onBindViewHolder(holder: ArticleItemHolder, position: Int) {
        val data = dataList[position]
        holder.title.text = data.title
        holder.loadImageFromUrl(data.urlToImage)
    }

    class ArticleItemHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        LoadImage.Delegate {
        val title = itemView.title
        val image = itemView.thumbnail_image



        fun loadImageFromUrl(imageUrl : String){
            val task = LoadImage(imageUrl,this)
            task.execute()
        }

        override fun onLoadFinish(bitmap: Bitmap) {
            image.setImageBitmap(bitmap)
        }
    }

    class LoadImage(val imageUrl: String,val delegate:Delegate): AsyncTask<String,Void,Bitmap>(){
        interface Delegate{
            fun onLoadFinish(bitmap: Bitmap)
        }


        override fun doInBackground(vararg p0: String?): Bitmap? {
            try {
                val url = URL(imageUrl)
                return BitmapFactory.decodeStream(url.openConnection().getInputStream())
            }catch (exception: Exception){
                println(exception.localizedMessage)
            }
            return null
        }

        override fun onPostExecute(result: Bitmap?) {
            super.onPostExecute(result)
            result?.let {
                delegate.onLoadFinish(it)
            }
        }

    }
}