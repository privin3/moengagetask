package com.privin.moengagetask.ui.home


import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.privin.moengagetask.R

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment(), HomeAdapter.HomeAdapterDelegate {


    private lateinit var adapter : HomeAdapter
    private lateinit var viewModel : HomeViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        setupRecyclerView(view)
        viewModel.getArticles().observe(this, Observer { list -> adapter.setData(list)  }  )
    }

    private fun setupRecyclerView(view:View){
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycler_view)
        adapter = HomeAdapter(this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(view.context,2)
    }

    override fun onItemClicked(position: Int) {

    }


}
